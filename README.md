# Boost Mastodon statuses

Intended to boost everyday a different status according to an input schedule.

1. Create a new Application in you Mastodon account https://your_instance.social/settings/applications
2. Edit the schedule of boosts `nano boosts.json`
4. Run the script on a daily basis:
   * manually from command line:
     * `export MASTODON_ACCESS_TOKEN=<your_token_from_step1>`
     * `export MASTODON_API_BASE_URL=<your_instance.social>`
     * `python main.py`
   * automatically with GitLab CI:
     * Settings > CI/CD > Variables > Set both environment variables on the protected branch `main` (cf [doc](https://docs.gitlab.com/ee/ci/variables/#for-a-project))
     * Settings > CI/CD > Variables > Build > Pipeline schedules > New schedule > Enable a daily run (cf [doc](https://docs.gitlab.com/ee/ci/pipelines/schedules.html))
     * Then click PLAY to test the pipeline once or wait for the next daily trigger

The script publishes once. Make sure you do not run the script several times a day.

